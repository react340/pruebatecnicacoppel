import React from "react";
import moment from 'moment'
import { ConsultaSKU, EnvioABaseDatos, ActualizarArticulo, EliminarArticulo }  from '../servidor/conexiones/ayuda_consultas';
import { ToastContainer, toast } from 'react-toastify';
import Swal from 'sweetalert2'
import { consulta_Fetch } from '../servidor/conexiones/server';

export default function AppPracticaCoppel() {
    // Arreglos que se acomodan con la búsqueda por SKU
    const [opciones_variables, setOpcionesVariables] = React.useState([])
    // Tienen un valor original sin filtrar proveniente de la base de datos para su consulta
    /*-->*/const [familia,setFamilia] = React.useState([]) 
    /*-->*/const [clase,setClase] = React.useState([])

    // Banderas declaradas para diferentes acciones 
    const [actualiza, setActualiza] = React.useState(0)
    const [activo, setActivo] = React.useState(true)

    // Fecha estática para los campos de fecha alta y fecha baja
    var dia_actual = moment().format('YYYY-MM-DD'); 

    // Arreglo donde se almacenan los datos que se van recuperando del formulario 
    const [recopila_datos, setRecopilaDatos] = React.useState({
        sku:'', articulo:'',marca:'',modelo:'',departamento:0, 
        clase:0, familia:0,stock:0, cantidad:0, descontinuado:'0', 
        fecha_alta:'', fecha_baja:''
    })
    const filtrar_clase = (event) =>{
        const filtro_clases = (opciones_variables.clase).filter((element)=> (element.clas_dep_prod === event.target.value)? element:false)
        // Define un nuevo arreglo de clases 
        setClase(filtro_clases)
        // Se agrega el campo del departamento al arreglo recopila_datos
        setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})
    }
    const filtrar_familia = (event) =>{
        const filtro_familia = (opciones_variables.familia).filter((element)=> 
            (element.fam_clas_prod === event.target.value &&  element.fam_dep_prod === recopila_datos.departamento)? 
        element:false)
        // Define un nuevo arreglo de clases 
        setFamilia(filtro_familia)
        // Se agrega el campo del clase al arreglo recopila_datos
        setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})
    }

    console.log(actualiza)
    return (
        <div className="flex place-items-center p-2">
            <div className="shadow-lg border-2 border-black">
                <div className="bg-black text-white p-2">
                    Form 1
                </div>
                <div>
                    <div className="flex flex-row justify-between">
                        <label className="flex p-2 w-96 place-items-center justify-between ">
                            <span>Sku:</span>
                            <input type="number" min="0" name="sku" onKeyUp={consultar_skuBD} className="border-2 outline-none w-48 p-1"/>
                        </label>
                        <label className="flex place-items-center justify-center space-x-3 w-96">
                            <span>Descontinuado</span>
                            <div className="p-1">
                                <button onClick={(event)=> setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})} 
                                name="descontinuado" value='1' disabled={activo} className={`cursor-pointer font-bold p-2 rounded-md w-16 text-xs ${(recopila_datos.descontinuado === '1')?'bg-blue-500 text-white':'bg-gray-200 text-gray-500'}`}
                                >Si</button>
                            </div>
                            <div className="p-1">
                                <button onClick={(event)=> setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})} name="descontinuado" value='0' disabled={activo} className={`cursor-pointer font-bold p-2 rounded-md w-16 text-xs ${(recopila_datos.descontinuado === '0')?'bg-blue-500 text-white':'bg-gray-200 text-gray-500'}`}>No</button>
                            </div>
                        </label>
                    </div>
                    <div className="grid grid-col justify-between ">
                        <label className="flex p-2 w-96 place-items-center justify-between ">
                            <span>Artículo:</span>
                            <input type="text" name="articulo" defaultValue={recopila_datos.articulo} 
                            onChange={(event)=> setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})} className="border-2 outline-none w-48 p-1"/>
                        </label>
                    </div>
                    <div className="grid grid-col justify-between ">
                        <label className="flex p-2 w-96 place-items-center justify-between ">
                            <span>Marca:</span>
                            <input type="text" defaultValue={recopila_datos.marca} name="marca" onChange={(event)=> setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})} className="border-2 outline-none w-48 p-1"/>
                        </label>
                    </div>
                    <div className="grid grid-col justify-between ">
                        <label className="flex p-2 w-96 place-items-center justify-between ">
                            <span>Modelo:</span>
                            <input type="text" defaultValue={recopila_datos.modelo} name="modelo" onChange={(event)=> setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})} className="border-2 outline-none w-48 p-1"/>
                        </label>
                    </div>
                    <div className="grid grid-col justify-between ">
                        <label className="flex p-2 w-96 place-items-center justify-between ">
                            <span>Departamento:</span>
                            <select name="departamento" value={recopila_datos.departamento}
                            onChange={filtrar_clase} className="border-2 outline-none w-48 p-1 disabled:opacity-75">
                                <option value="Selecciona un departamento">Selecciona un departamento</option>
                                {(opciones_variables.departamentos)?.map((json)=>{
                                    return <React.Fragment key={json.dep_id }>
                                        <option value={json.dep_numero}>{json.dep_nombre}</option>
                                    </React.Fragment>
                                })}
                            </select>
                        </label>
                    </div>
                    <div className="grid grid-col justify-between ">
                        <label className="flex p-2 w-96 place-items-center justify-between ">
                            <span>Clase:</span>
                            <select name="clase" 
                            onChange={filtrar_familia} value={recopila_datos.clase} className="border-2 outline-none w-48 p-1">
                                <option value="">Selecciona una clase</option>
                                {(clase)?.map((json)=>{
                                    return <React.Fragment key={json.clas_id }>
                                        <option value={json.clas_numero}>{json.clas_nombre}</option>
                                    </React.Fragment>
                                })}
                            </select>
                        </label>
                    </div>
                    <div className="grid grid-col justify-between ">
                        <label className="flex p-2 w-96 place-items-center justify-between ">
                            <span>Familia:</span>
                            <select name="familia" value={recopila_datos.familia}
                            onChange={(event)=>{setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})}} className="border-2 outline-none w-48 p-1">
                                <option value="">Selecciona una clase</option>
                                {(familia)?.map((json)=>{
                                    return <React.Fragment key={json.fam_id  }>
                                        <option value={json.fam_num}>{json.fam_nombre}</option>
                                    </React.Fragment>
                                })}
                            </select>
                        </label>
                    </div>
                    <div className="flex flex-row justify-between">
                        <div className="flex flex-col">
                            <label className="flex p-2 w-96 place-items-center justify-between ">
                                <span>Cantidad</span>
                                <input type="number" min="0" name="cantidad" value={recopila_datos.cantidad} onChange={(event)=> setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})} className={`border-2 outline-none w-48 p-1 ${(recopila_datos.stock) < (recopila_datos.cantidad) ?'border-red-500':false}`}/>
                            </label>
                            {
                                (recopila_datos.stock) < (recopila_datos.cantidad) ?
                                <div className="text-right text-red-500 w-full text-sm -mx-5">
                                    <span>La cantidad no puede ser mayor al stock</span>
                                </div>:false
                            }
                        </div>
                        <div className="flex flex-col">
                            <label className="flex p-2 w-96 place-items-center justify-between  ">
                                <span>Stock:</span>
                                <input type="number" min="0" name="stock" value={recopila_datos.stock} onChange={(event)=> setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})} className={`border-2 outline-none w-48 p-1 ${(recopila_datos.stock) < (recopila_datos.cantidad) ?'border-red-500':false}`}/>
                            </label>
                            {
                                (recopila_datos.stock) < (recopila_datos.cantidad) ?
                                <div className="text-right text-red-500 w-full text-sm -mx-5">
                                    <span>La cantidad no puede ser mayor al stock</span>
                                </div>:false
                            }
                        </div>
                    </div>
                    <div className="flex flex-row justify-between">
                        <label className="flex p-2 w-96 place-items-center justify-between  ">
                            <span>Fecha alta:</span>
                            <input type="date" disabled={true} className="border-2 outline-none w-48 p-1" value={(actualiza)? recopila_datos.fecha_alta: dia_actual}/>
                        </label>
                        <label className="flex p-2 w-96 place-items-center justify-between ">
                            <span>Fecha baja</span>
                            <input type="date" className="border-2 outline-none w-48 p-1" name="fecha_baja" onChange={(event)=> setRecopilaDatos({...recopila_datos, [event.target.name]:event.target.value})} disabled={true} value={(recopila_datos.descontinuado === '1')? dia_actual:recopila_datos.fecha_baja}/>
                        </label>
                    </div>
                    <div className="flex justify-between p-2 space-x-2">
                        {
                            (recopila_datos.sku !== '' && (recopila_datos.stock) >= (recopila_datos.cantidad))?
                            // Valida para que no inserte una cantidad mayor al stock
                            <React.Fragment>
                                <button onClick={EnviarActualizaciones} className="w-36 p-2 bg-blue-500 rounded-sm font-bold text-white hover:bg-blue-600">Actualizar</button>
                                <button onClick={EnviarDatosFormulario} className="w-36 p-2 bg-green-500 rounded-sm font-bold text-white hover:bg-green-600">Dar de alta</button>
                                <button onClick={EnviarEliminacionArticulo} className="w-36 p-2 bg-red-500 rounded-sm font-bold text-white hover:bg-red-600">Eliminar</button>
                            </React.Fragment>:false
                        }
                    </div>
                </div>
            </div>
            <ToastContainer position="bottom-right" autoClose={3000}
            hideProgressBar={false} newestOnTop={false}
            closeOnClick rtl={false}
            pauseOnFocusLoss draggable pauseOnHover
            theme="colored"/>
        </div>
    );

    async function consultar_skuBD(event) {
        event.preventDefault()
        var sku_ingresado = parseInt(event.target.value)
        const validar_numero = new RegExp("^[0-9]+[0-9]*$") //Valida si el numero es positivo sin decimales
        var resultado_validacion = validar_numero.test(event.target.value)
        if(sku_ingresado !== '' && event.keyCode === 13){  
            if(resultado_validacion){
                const va_sku = await ConsultaSKU({sku: event.target.value})
                setFamilia(va_sku.familia)
                setClase(va_sku.clases)
                setOpcionesVariables({
                    departamentos: va_sku.departamentos,
                    familia: va_sku.familia, 
                    clase: va_sku.clases
                })
                if(va_sku.resultado === "No existe el SKU"){
                    setRecopilaDatos({
                        sku:event.target.value, articulo:'',marca:'',modelo:'',departamento:0, 
                        clase:0, familia:0,stock:0, cantidad:0, descontinuado:false,  
                        fecha_alta:dia_actual, fecha_baja:''
                    })
                    setActivo(true)
                }else{
                    const resultado_encontrado = va_sku.resultado[0]
                    toast.info('Este SKU esta registrado, pero puedes actualizarlo.', {
                        position: "bottom-right", autoClose: 3000, 
                        hideProgressBar: false, closeOnClick: true, 
                        pauseOnHover: true, draggable: true, 
                        progress: undefined, theme: "colored",
                    });
                    setRecopilaDatos({
                        sku:event.target.value,
                        articulo: resultado_encontrado.articulo,
                        marca:resultado_encontrado.marca,modelo: resultado_encontrado.modelo,
                        departamento:resultado_encontrado.departamento, clase:resultado_encontrado.clase, 
                        familia:resultado_encontrado.familia,stock:resultado_encontrado.stock, 
                        cantidad:resultado_encontrado.cantidad,
                        fecha_alta: resultado_encontrado.fecha_alta, 
                        fecha_baja:resultado_encontrado.fecha_baja,
                        descontinuado: resultado_encontrado.descontinuado 
                    })
                    setActualiza(1)
                    setActivo(false)
                }
            }else{
                toast.error('Solo acepta números enteros sin decimales', {
                    position: "bottom-right", autoClose: 3000, 
                    hideProgressBar: false, closeOnClick: true, 
                    pauseOnHover: true, draggable: true, 
                    progress: undefined, theme: "colored",
                });
            }
        }
    }

    async function EnviarDatosFormulario(event){
        event.preventDefault()
        const envio_datos_f = await EnvioABaseDatos({datos: recopila_datos, fecha_a: dia_actual, fecha_b:'1900-01-01'})
        if(envio_datos_f === 'Exito'){
            toast.success('Artículo registrado con éxito', {
                position: "bottom-right", autoClose: 3000, 
                hideProgressBar: false, closeOnClick: true, 
                pauseOnHover: true, draggable: true, 
                progress: undefined, theme: "colored",
            });
            setInterval(() => {
                window.location.reload(false);
            }, 3000);
        }else{
            toast.error('Upss... Aquí hay algo mal', {
                position: "bottom-right", autoClose: 3000, 
                hideProgressBar: false, closeOnClick: true, 
                pauseOnHover: true, draggable: true, 
                progress: undefined, theme: "colored",
            });
        }
    }
    async function EnviarActualizaciones(event){
        event.preventDefault()
        Swal.fire({
            title: '¡Estás a punto de actualizar este artículo!',
            text: "¿Estas seguro de actualizarlo?",
            icon: 'info',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            confirmButtonColor: '#8BC34A',
            reverseButtons: true}).then(async(result) => {
            if (result.isConfirmed) {
                const baja_datos_f = await ActualizarArticulo({
                    sentencia: `UPDATE consulta_articulos SET sku=${recopila_datos.sku},articulo='${recopila_datos.articulo}',marca='${recopila_datos.marca}',modelo='${recopila_datos.modelo}',departamento='${recopila_datos.departamento}',clase='${recopila_datos.clase}',familia='${recopila_datos.familia}',stock='${recopila_datos.stock}',cantidad='${recopila_datos.cantidad}',descontinuado='${recopila_datos.descontinuado}',fecha_baja='${
                        (recopila_datos.descontinuado === '1')? dia_actual:'1900-01-01'
                    }' WHERE sku = '${recopila_datos.sku}'`
                })
              
                if(baja_datos_f === 'Exito'){
                    toast.success('Artículo actualizado con éxito', {
                        position: "bottom-right", autoClose: 3000, 
                        hideProgressBar: false, closeOnClick: true, 
                        pauseOnHover: true, draggable: true, 
                        progress: undefined, theme: "colored",
                    });
                    setInterval(() => {
                        window.location.reload(false);
                    }, 3000);
                }else{
                    toast.error('Upss... Aquí hay algo mal', {
                        position: "bottom-right", autoClose: 3000, 
                        hideProgressBar: false, closeOnClick: true, 
                        pauseOnHover: true, draggable: true, 
                        progress: undefined, theme: "colored",
                    });
                }
            } else {
                toast.info('¡Proceso cancelado!', {
                    position: "bottom-right", autoClose: 3000, 
                    hideProgressBar: false, closeOnClick: true, 
                    pauseOnHover: true, draggable: true, 
                    progress: undefined, theme: "colored",
                });
            }
        })
    }
    async function EnviarEliminacionArticulo(event){
        event.preventDefault()
        Swal.fire({
            title: '¡Estás a punto de eliminar este artículo!',
            text: "¿Estas seguro de eliminarlo?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            confirmButtonColor: '#8BC34A',
            reverseButtons: true}).then(async(result) => {
            if (result.isConfirmed) {
                const baja_datos_f = await consulta_Fetch(`app_mysql_delete.php`, { 
                    from: 'consulta_articulos',
                    insert_values: recopila_datos.sku
                })

                // Deja de pasar por el archivo ayuda_consultas.js en la funcion EliminarArticulo, 
                // para pasar al archivo app_mysql_delete.php mas seguido.

                // Tiempo de cambio: 15 min aprox

                /*const baja_datos_f = await EliminarArticulo({
                    sentencia: `DELETE FROM consulta_articulos WHERE sku = '${recopila_datos.sku}'`
                })*/

                        
                if(baja_datos_f.return.estatus === 'Exito'){
                    toast.success('Artículo eliminado con éxito', {
                        position: "bottom-right", autoClose: 3000, 
                        hideProgressBar: false, closeOnClick: true, 
                        pauseOnHover: true, draggable: true, 
                        progress: undefined, theme: "colored",
                    });
                    setInterval(() => {
                        window.location.reload(false);
                    }, 3000);
                }else{
                    toast.error('Upss... Aquí hay algo mal', {
                        position: "bottom-right", autoClose: 3000, 
                        hideProgressBar: false, closeOnClick: true, 
                        pauseOnHover: true, draggable: true, 
                        progress: undefined, theme: "colored",
                    });
                }
            } else {
                toast.info('¡Proceso cancelado!', {
                    position: "bottom-right", autoClose: 3000, 
                    hideProgressBar: false, closeOnClick: true, 
                    pauseOnHover: true, draggable: true, 
                    progress: undefined, theme: "colored",
                });
            }
        })
    }
}