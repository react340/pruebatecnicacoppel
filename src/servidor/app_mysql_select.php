<?php
require './conexiones/poo.php';

$funciones = new funciones_php();
$return = array(
    'estatus' => '',
    'comentarios' => '',
    'data' => ''
);
$data_consulta = $funciones->mysql_select($_POST['from'], $_POST['order_by'], isset($_POST['where'])? "WHERE $_POST[where]": '');
if ($data_consulta) {
    $return['estatus'] = "Exito:";
    $return['data'] = $data_consulta;
}else{
    $return['estatus'] = "Falla:";
    $return['data'] = [];
    $return['comentarios'] = "Lista no obtenida";
}

echo json_encode($return);

?>