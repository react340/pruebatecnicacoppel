<?php
require './conexiones/poo.php';

$funciones = new funciones_php();
$return = array(
    'estatus' => '',
    'comentarios' => '',
    'data' => []
);

$data = json_decode(file_get_contents('php://input'));
$data_consulta = $funciones->mysql_select_libre($data->query);
if ($data_consulta) {
    $return['estatus'] = "Exito:";
    $return['data'] = $data_consulta;
}else{
    $return['estatus'] = "Falla:";
    $return['data'] = [];
    $return['comentarios'] = "Lista no obtenida";
}

echo json_encode($return);

?>