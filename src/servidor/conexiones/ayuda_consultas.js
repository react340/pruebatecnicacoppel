import { consulta_Fetch } from '../conexiones/server';

export async function ConsultaSKU(props){
    var obtiene_informacion;
    try{
        const data_query = await consulta_Fetch(`app_mysql_libre.php`, {
            query: `SELECT * FROM  consulta_articulos WHERE sku = ${props.sku};`
        });

        const query_clase = await consulta_Fetch(`app_mysql_libre.php`, { query: `SELECT * FROM  clase;` });
        const query_departamento = await consulta_Fetch(`app_mysql_libre.php`, { query: `SELECT * FROM  departamento;` });
        const query_familia = await consulta_Fetch(`app_mysql_libre.php`, { query: `SELECT * FROM  familia;` });

        if(data_query.return.data[0] === undefined){  
            obtiene_informacion = 'No existe el SKU'
            return {resultado: obtiene_informacion, clases: query_clase.return.data, departamentos: query_departamento.return.data, familia: query_familia.return.data}
        }else{
            obtiene_informacion = data_query.return.data
            return {resultado: obtiene_informacion, clases: query_clase.return.data, departamentos: query_departamento.return.data, familia: query_familia.return.data}
        }
    }catch(e){
        return {mensaje:"Error 002"}
    }
}

export async function EnvioABaseDatos({datos, fecha_a, fecha_b}){
    try{
        const data = await consulta_Fetch(`app_mysql_insert.php`, {
            from: 'consulta_articulos',
            insert_values: ` null, ${datos.sku}, '${datos.articulo}', '${datos.marca}', '${datos.modelo}'
            , '${datos.departamento}', '${datos.clase}', '${datos.familia}', '${fecha_a}'
            , '${datos.stock}', '${datos.cantidad}', '${datos.descontinuado}', '${fecha_b}'`
        });
        if(data.estatus && data.return.estatus.length > 0 && data.return.estatus.indexOf('Error:') === -1){
            return "Exito"
        }else{
            return console.log(data), 
            console.log(data.return.estatus)
        }
    }catch(e){
        return "Error 002"
    }
}

export async function ActualizarArticulo(props){
    try{
        const data = await consulta_Fetch(`app_mysql_libre.php`, { query: props.sentencia });
        if(data.return.estatus.length > 0){
            return "Exito"
        }else{
            return console.log(data.return.estatus)
        }
    }catch(e){
        return "Error 002"
    }

}


// Esta funcion quedaria en desuso porque se ingresa la consulta fetch directamente desde el app.jsx enviando UNICAMENTE el paramentro de la tabla y el SKU
export async function EliminarArticulo(props){
    try{
        const data = await consulta_Fetch(`app_mysql_libre.php`, { query: props.sentencia });
        if(data.return.estatus.length > 0){
            return "Exito"
        }else{
            return console.log(data.return.estatus)
        }
    }catch(e){
        return "Error 002"
    }

}
