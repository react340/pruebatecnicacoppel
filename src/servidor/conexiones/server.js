export async function consulta_Fetch(ruta, data_body = {}){
    try{
        const data = await fetch(`http://localhost/PruebaTecnicaCoppel/src/servidor/${ruta}`, {
            method: "POST",
            body: JSON.stringify(data_body)
        });
        var responseJson = await data.json();
        return {
            return: responseJson,
            estatus: true
        };
    }catch(e){
        return {
            estatus: false,
            return: {
                estatus: 'falla',
                comentarios: e.message
            }
        };
    }
}