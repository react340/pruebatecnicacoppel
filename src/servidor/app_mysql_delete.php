<?php
require './conexiones/poo.php';

$funciones = new funciones_php();
$return = array(
    'estatus' => '',
    'comentarios' => '',
    'data' => null
);


//Similar al de mysql_insert sin embargo apunta a una funcion que es para eliminar datos

$data = json_decode(file_get_contents('php://input'));
$data_insert = $funciones->mysql_delete($data->from, $data->insert_values);
if ($data_insert) {
    $return['estatus'] = "Exito";
    $return['comentarios'] = "El elemento se ha eliminado";
}else{
    $return['estatus'] = "Error:";
    $return['comentarios'] = "No se creo el elemento";
};

echo json_encode($return);

?>