import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './componente/App';
import './index.css';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
