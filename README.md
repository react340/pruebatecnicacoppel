# ¡Hola!, que bueno que esta aqui


Primero que nada, gracias por darme la oportunidad de poder aplicar para la vacante y enviarme el examen práctico para poder realizarlo. 

Aquí unos puntos que hay que considerar si va a probar este desarrollo en entorno local:


# Modificaciones antes de correr el proyecto
* Debe de instalar la carpeta node_modules para que se pueda ejecutar, para ello debemos de haber instalado NodeJs y npm con anterioridad. Dentro de la carpeta del proyecto vía consola debemos de descargar la carpeta usando "npm i"
* En el archivo server.js (/src/servidor/conexiones/server.js) hay un parámetro llamado "data" que hace un fecth a una url, esta url es donde hará todas las consultas que se requiera, por lo que la liga debe de ser modificada acorde a donde se almacena el proyecto. Ej: "http://localhost/nombre_de_carpeta_contenedora/src/servidor/${ruta}"
* Para la base de datos, aquí mismo se adjunta el .sql para que pueda ser ejecutado, el nombre de la base de datos es: **ejercicio_practico_coppel**, pero si se agrega otro nombre, debe de ser modificado dentro de poo.php (/src/servidor/conexiones/poo.php) en la parte de "$BD_BD"

# ¿Que hacer si ya acomode los parametros anteriores?
Lo que queda es correr el programa, desde la terminal en el inicio de todo el programa, se debe de ejecutar "npm start"




### CAMBIO REALIZADO Y SOLICITADO

*MODIFICAR LA PARTE DE LA ELIMINACION COMO PRUEBA DE QUE EL CODIGO SE PUEDE MODIFICAR*

Archivos modificados: 
- Poo.php
- App.jsx

Archivos agregados: 
- app_mysql_delete.php



