-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2022 a las 19:21:16
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ejercicio_practico_coppel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase`
--

CREATE TABLE `clase` (
  `clas_id` int(11) NOT NULL,
  `clas_numero` int(3) NOT NULL,
  `clas_nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `clas_dep_prod` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clase`
--

INSERT INTO `clase` (`clas_id`, `clas_numero`, `clas_nombre`, `clas_dep_prod`) VALUES
(1, 1, 'Comestibles', 1),
(2, 1, 'Comestibles', 1),
(3, 2, 'Licuadoras', 1),
(4, 3, 'Batidoras', 1),
(5, 4, 'Cafeteras', 1),
(6, 1, 'Amplificadores Car Audio', 2),
(7, 2, 'Auto stereos', 2),
(8, 1, 'Colchon', 3),
(9, 2, 'Juego box', 3),
(10, 1, 'Salas', 4),
(11, 2, 'Complementos para sala', 4),
(12, 3, 'Sofas cama', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consulta_articulos`
--

CREATE TABLE `consulta_articulos` (
  `id` int(11) NOT NULL,
  `sku` int(6) NOT NULL,
  `articulo` text COLLATE utf8_spanish_ci NOT NULL,
  `marca` text COLLATE utf8_spanish_ci NOT NULL,
  `modelo` text COLLATE utf8_spanish_ci NOT NULL,
  `departamento` int(1) NOT NULL,
  `clase` int(2) NOT NULL,
  `familia` int(3) NOT NULL,
  `fecha_alta` date NOT NULL,
  `stock` int(9) NOT NULL,
  `cantidad` int(9) NOT NULL,
  `descontinuado` int(1) DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `consulta_articulos`
--

INSERT INTO `consulta_articulos` (`id`, `sku`, `articulo`, `marca`, `modelo`, `departamento`, `clase`, `familia`, `fecha_alta`, `stock`, `cantidad`, `descontinuado`, `fecha_baja`) VALUES
(1, 5, '1', '2', '2', 3, 1, 3, '2022-11-08', 50, 10, 0, '1900-01-01'),
(5, 3, '1', '1', '1', 4, 2, 1, '2022-11-08', 55, 55, 0, '1900-01-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `dep_id` int(11) NOT NULL,
  `dep_numero` int(3) NOT NULL,
  `dep_nombre` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`dep_id`, `dep_numero`, `dep_nombre`) VALUES
(1, 1, 'Domesticos'),
(2, 2, 'Electronica'),
(3, 3, 'Mueble suelto'),
(4, 4, 'Salas, recamaras, comedores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familia`
--

CREATE TABLE `familia` (
  `fam_id` int(11) NOT NULL,
  `fam_num` int(3) NOT NULL,
  `fam_nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `fam_clas_prod` int(3) NOT NULL,
  `fam_dep_prod` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `familia`
--

INSERT INTO `familia` (`fam_id`, `fam_num`, `fam_nombre`, `fam_clas_prod`, `fam_dep_prod`) VALUES
(1, 0, 'Sin nombre', 1, 1),
(2, 1, 'Licuadoras', 2, 1),
(3, 2, 'Exlusivo Coppel.com', 2, 1),
(4, 1, 'Batidora manual', 3, 1),
(5, 2, 'Procesador', 3, 1),
(6, 3, 'Picadora', 3, 1),
(7, 4, 'Batidora pedestal', 3, 1),
(8, 5, 'Batidora fuente de so', 3, 1),
(9, 6, 'Multipracticos', 3, 1),
(10, 7, 'Exclusivo Coppel.com', 3, 1),
(11, 1, 'Cafeteras', 4, 1),
(12, 2, 'Percoladoras', 4, 1),
(13, 1, 'Amplificador/Receptor', 1, 2),
(14, 2, 'Kit de instalacion', 1, 2),
(15, 3, 'Amplificadores Coppel', 1, 2),
(16, 1, 'Autoestereo CD C/Bocina', 2, 2),
(17, 2, 'Accesorios Car Audio', 2, 2),
(18, 3, 'Amplificador', 2, 2),
(19, 4, 'Alarma/casa/oficina', 2, 2),
(20, 5, 'Sin mecanismo', 2, 2),
(21, 6, 'Con CD', 2, 2),
(22, 7, 'Multimedia', 2, 2),
(23, 8, 'Paquete sin mecanismo', 2, 2),
(24, 9, 'Paquete con CD', 2, 2),
(25, 1, 'Pillow Top KS', 1, 3),
(26, 2, 'Pillow Top doble KS', 1, 3),
(27, 3, 'Hule espuma KS', 1, 3),
(28, 1, 'Estandar individual', 2, 3),
(29, 2, 'Pillow Top individual', 2, 3),
(30, 3, 'Pillow top doble ind', 2, 3),
(31, 1, 'Esquineras superiores', 1, 4),
(32, 2, 'Tipo L seleccional', 1, 4),
(33, 1, 'Sillon ocacional', 2, 4),
(34, 2, 'Puff', 2, 4),
(35, 3, 'Baul', 2, 4),
(36, 4, 'Taburete', 2, 4),
(37, 1, 'Sofa cama patizado', 3, 4),
(38, 2, 'Sofacama clasico', 3, 4),
(39, 3, 'Estudio', 3, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clase`
--
ALTER TABLE `clase`
  ADD PRIMARY KEY (`clas_id`);

--
-- Indices de la tabla `consulta_articulos`
--
ALTER TABLE `consulta_articulos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`dep_id`);

--
-- Indices de la tabla `familia`
--
ALTER TABLE `familia`
  ADD PRIMARY KEY (`fam_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clase`
--
ALTER TABLE `clase`
  MODIFY `clas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `consulta_articulos`
--
ALTER TABLE `consulta_articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `familia`
--
ALTER TABLE `familia`
  MODIFY `fam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
